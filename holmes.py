"""
Interface to holmespy libraries and client models for the AFRL-GEL project.
"""

import json
import matplotlib.pyplot as plt
import numpy as np
import pickle
import pprint
import sys
import timeit

from holmespy.beliefmodels import GaussianProcessBeliefModel
from holmespy.alternativesets import ContinuousAlternativeSet
from holmespy.optimizers import ContinuousOptimizer, BayesianOptimizer
from holmespy.state import BayesianOptimizationState
from holmespy.policies import GeneralizedKGPolicy, EIPolicy
from holmespy.simulator import Simulator, SimulationParameters

ACTION = "suggest"

def suggest(params):
    """
    Suggests an experiment to run according to the generalized KG policy.

    If params['kg_calibrate_params'] is set to true, then KG parameters
    are calibrated before running the KG policy algorithm. In this case,
    any kg parameters specified in params may be ignored. Otherwise, the 
    parameter values in params are used.
    """

    # build problem
    prob = _build_problem_from_params(params)

    # calibrate kg parameters
    if(params['kg_calibrate_params']):
        p.p('> Calibrating KG parameters:')
        calibrated_params = _calibrate_kg_params(prob, 
                params['kg_calibration_num_points'],
                params['kg_calibration_num_samples'],
                params['kg_num_mc_sims'])
    else:
        calibrated_params = params

    policy = _build_policy(prob, 
            params['kg_num_mc_sims'],
            params['kg_opt_num_iter'],
            calibrated_params['kg_mu_0'],
            calibrated_params['kg_sigma_0'],
            calibrated_params['kg_sigma_W'])

    p.p('> Calculating suggested experiment:')
    return policy.suggest(prob['initial_state'])


def analyze_kg_properties(params):
    """
    Analyze statistical properties of the KG utility function calculated
    by Monte Carlo Simulation.

    This analysis is specific to the prior belief with data loaded from the
    data file specified in params, in addition to the afrl-gel state to vector
    function. The analysis determines the distribution kg values at various
    points in the input space as a function of number of monte carlo
    simulations used to calculate the expectations needed for the KG value.
    These distributions are then summarized in various plots, and calibrated KG
    policy/optimizer parameters are suggested.

    This analysis is used to test our assumptions about the noise model
    for the KG function, as well as to obtain priors for this function. In
    particular, this can be used to provide estimates for:
    * ``kg_mu_0``, 
    * ``kg_sigma_0``,
    * ``kg_simga_W``, 
    * ``kg_calibration_num_samples`` and 
    * ``kg_calibration_num_point``
    """

    analyze_params = params['analyze_kg_properties_params']

    # -- Sample kg values, or load them from file ---
    if 'infile' not in analyze_params or analyze_params['infile'] is None:
        # Calculate samples from scratch
        p.p("> Running simulations:")
        prob = _build_problem_from_params(params)
        num_points = params['kg_calibration_num_points']
        num_samples_per_point = params['kg_calibration_num_samples']

        log_min_num_sims = analyze_params['log_min_num_sims']
        log_max_num_sims = analyze_params['log_max_num_sims']
        num_sims_arr = [2**v for v in range(log_min_num_sims, log_max_num_sims)]

        kg_vals, X_sample = _simulate_kg_vals(prob, num_points,
                num_samples_per_point, num_sims_arr = num_sims_arr)

    else:
        # Load samples from data file
        p.p("> Loading simulation data file %s." % infile)
        with open(analyze_params['infile'], "rb") as fp:
            data = pickle.load(fp)

            kg_vals = data['kg_vals']
            num_sims_arr = data['num_sims_arr']
            X_sample = data['X_sample']


    # -- Plot summary statistics --
    _plot_kg_sample_stats(kg_vals, num_sims_arr, 
            plot_prefix = analyze_params['plot_prefix'])

    # -- Calculate calibrated value --
    # Average over all values where num sims is maximum
    kg_mu_0 = np.mean(kg_vals[-1,:,:])
    # Get a multiple of all observed values
    kg_sigma_0 = 5.0*np.std(kg_vals[-1,:,:])
    # how much variance do we see overall
    kg_sigma_W = np.max(np.std(kg_vals, axis = 2) , axis = 1)

    p.p("> Calibrated KG Parameters:")
    print("\t   kg_mu_0 = %5.3f" % kg_mu_0)
    print("\tkg_sigma_0 = %5.3f" % kg_sigma_0)
    print("\tkg_sigma_W : ", end = "")
    print(kg_sigma_W)
    print("\t  num_sims : ", end = "")
    print(num_sims_arr)

    # -- Write samples to data file if requested --
    if 'outfile' in analyze_params and analyze_params['outfile'] is not None:
        p.p("> Writing simulation data to file %s." % analyze_params['outfile'])
        with open(analyze_params['outfile'], "wb") as fp:
            pickle.dump({
                'kg_vals': kg_vals,
                'num_sims_arr': num_sims_arr,
                'X_sample': X_sample,
                'params': params}, fp)


def analyze_kg_opt(params):
    """
    These the role in the number of iterations used by the Bayesian optimizer
    to find optimal MCGKG values. 

    This analysis is used to study the ``kg_num_mc_sims`` parameter, and to
    provide a good value for it specific to the current belief states.
    """

    analyze_params = params['analyze_kg_opt_params']

    if 'infile' not in analyze_params or analyze_params['infile'] is None:
        # Run calculations from scratch
        p.p("> Running simulations:")

        max_num_iter = analyze_params['max_num_iter']
        min_num_iter = analyze_params['min_num_iter']
        num_num_iter = analyze_params['num_num_iter']
        num_sims_per_num_iter = analyze_params['num_sims_per_num_iter']

        prob = _build_problem_from_params(params)
        dI = int((max_num_iter- min_num_iter)/(num_num_iter - 1))
        num_iter_arr = [II for II in range(min_num_iter, max_num_iter, dI)]

        print(num_iter_arr)

        best_vals = _simulate_optimal_kg_vals(prob, num_iter_arr,
                num_sims_per_num_iter, params['kg_num_mc_sims'],
                params['kg_mu_0'], params['kg_sigma_0'], params['kg_sigma_W'])

    else:
        # Load data file 
        with open(analyze_params['infile'], "rb") as fp:
            data = pickle.load(fp)
            best_vals = data['best_vals']
            prob = data['prob']
            num_iter_arr = data['num_iter_arr']

    # 0-1 normalize data
    max_val = np.max(best_vals)
    min_val = np.min(best_vals)
    vals = (best_vals - min_val)/(max_val - min_val)

    # plot distribution of best vals vs. number of iter
    median_val = np.median(vals, axis = 1)
    lo_val = np.percentile(vals, 33, axis = 1)
    hi_val = np.percentile(vals, 67, axis = 1)

    plt.fill_between(num_iter_arr, lo_val, hi_val, alpha = 0.1, color = 'k')
    plt.plot(num_iter_arr, median_val)
    plt.xticks(num_iter_arr)
    plt.xlabel('Number of Bayesian Optimization iterations')
    plt.ylabel('Rel. value of optimal value')

    plt.grid(axis = 'y')
    plt.savefig("%s_opt_val_vs_num_opt_steps" % analyze_params['plot_prefix'])

    if 'outfile' in analyze_params and analyze_params['outfile'] is not None:
        p.p("> Writing simulation data to file %s." % analyze_params['outfile'])
        with open(analyze_params['outfile'], "wb") as fp:
            pickle.dump({
                'best_vals': best_vals,
                'prob': prob,
                'num_iter_arr': num_iter_arr,
                'params': params}, fp)


# --- AUX FUNCTIONS ---

def _calibrate_kg_params(prob, num_points, num_samples_per_point, num_sims):

    """
    Calculates optimal values for KG parameters.

    Givena problem definition, and calibration definitions, this function
    calculates sample value for the parameters kg_mu_0, kg_sigma_0, and
    kg_sigma_W 
    """

    # Sample kg values
    kg_vals, _ = _simulate_kg_vals(prob, num_points, num_samples_per_point, 
        num_sims_arr = [num_sims])
    kg_vals = np.squeeze(kg_vals)

    # Calculate calibrated parameters:
    mean_kg_values = np.mean(kg_vals, axis = 1)
    std_kg_values = np.std(kg_vals, axis = 1)
    assert(len(mean_kg_values) == num_points)
    assert(len(std_kg_values) == num_points)

    # kg_mu_0 = mean of all values
    kg_mu_0 = np.mean(mean_kg_values)

    # kg_sigma_0 = use multiple of standard deviation of means
    kg_sigma_0  = 5.0*np.std(mean_kg_values)
        
    # kg_sigma_W = use max of standard deviation
    kg_sigma_W = np.max(std_kg_values)

    res = {
        # kg_mu_0 = use mean of all values
        'kg_mu_0': np.mean(mean_kg_values),
        # kg_sigma_0 = use multiple of std observed in sample
        'kg_sigma_0': 5.0*np.std(mean_kg_values),
        # kg_sigma_W = use max of standard deviation
        'kg_sigma_W': np.max(std_kg_values),
        # the raw data
        'kg_vals': kg_vals
    }

    p.p("\n> Calibrated parameters:")
    p.p("\t   kg_mu_0: %5.3f" % res['kg_mu_0'])
    p.p("\tkg_sigma_0: %5.3f" % res['kg_sigma_0'])
    p.p("\tkg_sigma_W: %5.3f" % res['kg_sigma_W'])
    p.p(" ")

    return res
        

def _build_problem_from_params(params):
    return _build_problem( params['input_var_bounds'], params['mu_0'],
            params['sigma_0'], params['sigma_W'], params['phase_lo'],
            params['phase_hi'], params['experiment_budget'],
            params['num_support_points'], params['ls_percent'],
            params['num_grid_steps'], params['exp_history_data_file'])


def _build_problem(input_var_bounds, mu_0, sigma_0, sigma_W,
    phase_lo, phase_hi, experiment_budget, 
    num_support_points, ls_percent, num_grid_steps,
    data_file_name = None):

    sigma2_W = sigma_W**2
    X_all = ContinuousAlternativeSet(input_var_bounds)
    aux_optimizer = ContinuousOptimizer(X_all)

    # --- Define initial state ----

    # Load preliminary data and add column for measurement noise
    if(data_file_name is not None):
        data = np.genfromtxt(data_file_name, delimiter = ',')[1:, 2:]
        num_data = data.shape[0]
        data = np.append(data, sigma2_W*np.ones([num_data, 1]), axis=1)
    else:
        data = None

    # auxillary parameters for GP
    X_support = np.array([X_all.sample() for _ in range(num_support_points)])
    length_scales = [ ls_percent*(bounds[1] - bounds[0]) \
                        for bounds in input_var_bounds]
    noise_variance_f = ConstantNoiseModel(sigma_W)

    # Bayesian prior and initial MDP state
    B0 = GaussianProcessBeliefModel( mu_0, sigma_0**2, length_scales,
            X_support, data = data) 
    initial_state = BayesianOptimizationState(B0, 0, {
        'experiment_budget': experiment_budget,
        'actions': X_all,
        'optimizer': aux_optimizer,
        'noise_variance_f': noise_variance_f
    })

    # --- Problem specific auxillary parameters for policy ----

    # build gridded points for use in state to vec map
    grid_vals = []
    for bounds in input_var_bounds:
        grid_vals.append(
                np.linspace(bounds[0], bounds[1], num_grid_steps))
    meshes = np.meshgrid(*grid_vals)
    flats = [np.ravel(mesh) for mesh in meshes]
    X_grid = np.array([x for x in zip(*flats)])
    phase_vec_f = PhaseVec(X_grid, phase_lo, phase_hi)

    return {
        'X_all': X_all,
        'X_grid': X_grid,
        'X_support': X_support,
        'noise_variance_f': noise_variance_f,
        'phase_vec_f': phase_vec_f,
        'aux_optimizer': aux_optimizer,
        'initial_state': initial_state,
        'length_scales': length_scales,
    }


def _build_policy_from_params(problem, params):
    return _build_policy(problem, params['kg_num_mc_sims'], 
            params['kg_mu_0'], params['kg_sigma_0'],
            params['kg_sigma_W'])


def _build_policy(problem, kg_num_mc_sims, kg_opt_num_iter, 
        kg_mu_0, kg_sigma_0, kg_sigma_W):

    # Build optimizer for KG value function
    kg_optimizer = _build_kg_optimizer(problem,
            kg_opt_num_iter, kg_mu_0, kg_sigma_0, kg_sigma_W)

    kg_policy = GeneralizedKGPolicy(kg_optimizer, 
        problem['noise_variance_f'], 
        problem['phase_vec_f'], 
        num_sims = kg_num_mc_sims)

    return kg_policy


def _build_kg_optimizer(problem, kg_opt_num_iter, kg_mu_0, kg_sigma_0,
    kg_sigma_W):

    kg_noise_variance_f = ConstantNoiseModel(kg_sigma_W)
    kg_optimizer_policy = EIPolicy(problem['aux_optimizer'], 
            kg_noise_variance_f)
    kg_B0 = GaussianProcessBeliefModel(kg_mu_0,
            kg_sigma_0**2, problem['length_scales'], problem['X_support'])
    kg_optimizer = BayesianOptimizer( problem['X_all'], 
            problem['aux_optimizer'], kg_optimizer_policy, kg_B0, 
            kg_noise_variance_f, num_iter = kg_opt_num_iter)

    return kg_optimizer


def _simulate_kg_vals(prob, num_points, num_samples_per_point, num_sims_arr):
    X_sample = [prob['X_all'].sample() for _ in range(num_points)]
    kg_vals = np.zeros([len(num_sims_arr), num_points, num_samples_per_point])

    num_evals = len(num_sims_arr)*num_points*num_samples_per_point
    eval_count = 0

    for i, num_sims in enumerate(num_sims_arr):
        kg_f = GeneralizedKGPolicy._KGUtility(prob['initial_state'],
                prob['noise_variance_f'], prob['phase_vec_f'], num_sims)
        for j,x in enumerate(X_sample):
            for k in range(num_samples_per_point):
                kg_vals[i,j,k] = kg_f(x)
                eval_count += 1
                p.p("\r\tSampling: %6.2f%%" % (100.0*eval_count/num_evals),
                    flush = True, end = "")

    p.p(" ")

    return kg_vals, X_sample


def _simulate_optimal_kg_vals(prob, num_iter_arr, num_sims, kg_num_mc_sims, 
        kg_mu_0, kg_sigma_0, kg_sigma_W):

    num_steps = len(num_iter_arr)

    # kg function
    kg_f = GeneralizedKGPolicy._KGUtility(prob['initial_state'],
            prob['noise_variance_f'], prob['phase_vec_f'], 
            kg_num_mc_sims)


    # The observed kg values
    best_vals = np.zeros([num_steps, num_sims])

    # for printing
    num_eval = num_steps * num_sims
    count = 0

    for i, num_iter in enumerate(num_iter_arr):
        # build optimizer with num_iter iterations
        kg_optimizer = _build_kg_optimizer(prob, num_iter, kg_mu_0, 
                kg_sigma_0, kg_sigma_W)

        # Get several values for statistics
        for j in range(num_sims):
            val, _ = kg_optimizer.optimize(kg_f)
            best_vals[i, j] = val

            count += 1
            p.p("\r\tOptimizing: %5.1f%%" % (100.0*count/num_eval),
                    flush = True, end = "")

    p.p(" ")

    return best_vals


def _plot_kg_sample_stats(kg_vals, num_sims_arr, plot_prefix = "kg_stats"):
    num_num_sims = kg_vals.shape[0]
    num_points = kg_vals.shape[1]
    num_samples_per_point = kg_vals.shape[2]

    # 1. What is the variability of the KG utility function (i.e. the
    # kg_sigma_W value)? How does it change with respect to: 
    # - number of # simulations used to calculate KG values?
    # - the input x value?

    # Overall box plot
    plt.figure(figsize = [10, 1 + 2*num_points])
    for j in range(num_points):
        plt.subplot(num_points, 1, j+1)
        plt.boxplot(np.squeeze(kg_vals[:,j,:]).T)
        plt.xticks(range(1, num_num_sims+1), num_sims_arr)
    plt.tight_layout()
    plt.savefig("%s__box_plots" % plot_prefix)

    # 2. What is the kg variablility over all points? Do we have a constant
    # noise model for KG values?
    std = np.zeros([num_num_sims, num_points])
    mean = np.zeros([num_num_sims, num_points])
    median = np.zeros([num_num_sims, num_points])
    for i in range(num_num_sims):
        for j in range(num_points):
            std[i,j] = np.std(kg_vals[i,j,:])
            mean[i,j] = np.mean(kg_vals[i,j,:])
            median[i,j] = np.median(kg_vals[i,j,:])

    plt.figure(figsize = [1 + 2*num_num_sims, 1 + 0.5*num_points])

    plt.subplot(1,3,1)
    _plot_summary_stats(std, num_sims_arr, 'Standard Deviation')
    plt.subplot(1,3,2)
    _plot_summary_stats(mean, num_sims_arr, 'Mean')
    plt.subplot(1,3,3)
    _plot_summary_stats(median, num_sims_arr, 'Median')
    plt.tight_layout()
    plt.savefig("%s__summaries" % plot_prefix)

def _plot_summary_stats(stats, num_sims_arr, stat_name):
    num_points = stats.shape[1]
    num_num_sims = stats.shape[0]

    # plot raw data
    plt.imshow(stats.T, aspect='auto')

    # annotate each box
    for i in range(num_points):
        for j in range(num_num_sims):
            plt.text(j, i, "%4.2f" % stats[j, i], 
                    ha="center", va="center", color = "w")

    plt.xticks(np.arange(num_num_sims), num_sims_arr)
    plt.xlabel('Monte Carlo simulation size')
    plt.yticks(np.arange(num_points), np.arange(num_points))
    plt.ylabel('Point index')
    plt.title(stat_name)



# --- Client Models ---

class PhaseVec:
    """
    Maps state to a phase vector

    The callable class that maps a Belief State to a 3D bitmap where each voxel
    represents a region in design space that holds either a "1" if that region
    belongs to the phase of interest and "0" otherwise.

    Args:
        sample_points (``iterable``): a list of sample points in design space 
            on which to calculate the corresponding phase.
        phase_lo (``float``): The lower bound of the gelation time that marks
            the positive phase
        phase_hi (``float``): The upper bound of the gelation time that makrs
            the positive phase
            
    """

    def __init__(self, sample_points, phase_lo, phase_hi):
        self.sample_points = sample_points
        self.phase_lo = phase_lo
        self.phase_hi = phase_hi

    def __call__(self, state):
        # Calculate the mean estimate of the gelation time at each of the
        # sampled points. Then map to 1 or 0 depending on thresholds
        vals = [state.current_beliefs.mean(x) for x in self.sample_points]
        bits = [ 1 if (y > self.phase_lo) and (y < self.phase_hi) else 0 \
                   for  y in vals]
        return np.array(bits)

class ConstantNoiseModel:
    def __init__(self, sigma_W):
        self.sigma2_W = sigma_W**2

    def __call__(self, x):
        return self.sigma2_W


# --- CLI Tool ---

class Logger:
    def __init__(self, verbose):
        self.verbose = verbose
        self.pp = pprint.PrettyPrinter(indent=4)

    def p(self, s,  flush = False, end = "\n", pretty = False):
        if(self.verbose):
            if pretty:
                __pp__.pprint(s)
            else:
                print(s, flush = flush, end = end)

def command_line_driver(action, params_file_name, verbose = False):
    """
    Driver for command line tool
    """

    p.verbose = verbose

    with open(params_file_name) as fp:
        params = json.load(fp)

    tic = timeit.default_timer()
    if action == "suggest":
        suggestion = suggest(params)
        print(suggestion)


    elif action == "analyze_kg_properties":
        analyze_kg_properties(params)

    elif action == "analyze_kg_opt":
        analyze_kg_opt(params)

    toc = timeit.default_timer()
    p.p("\nElapsed Time: %f" % (toc - tic))

p = Logger(False)
if __name__ == "__main__":
    command_line_driver(ACTION, sys.argv[1], verbose = True)
